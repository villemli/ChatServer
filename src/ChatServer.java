import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {
    public void serve() {
        ServerSocket serverSocket;
        Socket clientSocket;

        try {
            serverSocket = new ServerSocket(0);
            ChatInterpreter interpreter;
            Thread thread;
            System.out.println("Serving at port " + serverSocket.getLocalPort());

            while (true) {
                clientSocket = serverSocket.accept();
                interpreter = new ChatInterpreter(
                        clientSocket.getInputStream(),
                        new PrintStream(clientSocket.getOutputStream()));
                thread = new Thread(interpreter);
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
