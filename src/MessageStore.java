import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MessageStore {
    private List<ChatMessage> messages;
    private Set<MessageStoreObserver> observers;

    private MessageStore() {
        messages = new ArrayList<>();
        observers = new HashSet<>();
    }

    public void addMessage(ChatMessage message) {
        messages.add(message);
        observers.forEach(o -> o.newMessage(message));
    }

    public void deRegister(MessageStoreObserver observer) {
        observers.remove(observer);
    }

    public static MessageStore getInstance() {
        return MessageStoreHolder.INSTANCE;
    }

    public boolean register(MessageStoreObserver observer) {
        Set<String> usernames = observers
                .stream()
                .map(MessageStoreObserver::getUsername)
                .collect(Collectors.toSet());

        if (usernames.contains(observer.getUsername())) {
            return false;
        } else {
            observers.add(observer);
            return true;
        }
    }

    public String display(DateTimeFormatter formatter) {
        return messages
                .stream()
                .map(msg -> msg.display(formatter))
                .collect(Collectors.joining("\n"));
    }

    private static class MessageStoreHolder {
        private static final MessageStore INSTANCE = new MessageStore();
    }
}
