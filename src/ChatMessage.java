import java.time.Instant;
import java.time.format.DateTimeFormatter;

public class ChatMessage {
    private final Instant timestamp;
    private final String user;
    private final String contents;

    ChatMessage(Instant timestamp, String user, String contents) {
        this.timestamp = timestamp;
        this.user = user;
        this.contents = contents;
    }

    String display(DateTimeFormatter formatter) {
        return String.format("[%s] %s: %s", formatter.format(timestamp), user, contents);
    }
}
