public interface MessageStoreObserver {
    String getUsername();
    void newMessage(ChatMessage message);
}
