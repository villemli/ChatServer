import java.io.InputStream;
import java.io.PrintStream;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Scanner;

public class ChatInterpreter implements Runnable, MessageStoreObserver {
    private final PrintStream out;
    private final Scanner scanner;
    private final MessageStore messageStore;
    private String username;
    private DateTimeFormatter dateTimeFormatter;

    ChatInterpreter(InputStream in, PrintStream out) {
        this.out = out;
        scanner = new Scanner(in);
        messageStore = MessageStore.getInstance();
        dateTimeFormatter = DateTimeFormatter
                .ofLocalizedDateTime(FormatStyle.MEDIUM)
                .withZone(ZoneId.systemDefault());
    }

    private void cleanUp() {
        messageStore.deRegister(this);
        out.close();
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void newMessage(ChatMessage message) {
        out.println(message.display(dateTimeFormatter));
    }

    @Override
    public void run() {
        String line;

        while (true) {
            out.print("Please enter a username (empty to quit): ");
            username = scanner.nextLine();

            if (username.isEmpty()) {
                cleanUp();
                return;
            }

            if (!messageStore.register(this)) {
                out.println("Error: username is already taken");
            } else {
                break;
            }
        }

        out.println("Welcome to the chat, " + username + "!");

        do {
            line = writeMessage();
        } while (!line.equals(":quit"));

        cleanUp();
    }

    private String writeMessage() {
        String line;
        ChatMessage chatMessage;

        out.print("> ");
        line = scanner.nextLine();

        if (!line.startsWith(":")) {
            chatMessage = new ChatMessage(Instant.now(), username, line);
            messageStore.addMessage(chatMessage);
        } else if (line.equals(":print")) {
            out.println(messageStore.display(dateTimeFormatter));
        }

        return line;
    }
}
